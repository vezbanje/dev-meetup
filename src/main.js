import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import {store} from './store'
import DateFilter from './filters/date'
import Alert from './components/shared/Alert.vue'
import EditeMeetupDetailsDialog from './components/meetup/edit/EditMeetupDetailsDialog.vue'
import EditMeetupDateDialog from './components/meetup/edit/EditMeetupDateDialog.vue'
import EditMeetupTimeDialog from './components/meetup/edit/EditMeetupTimeDialog.vue'
import RegisterDialog from './components/meetup/registration/RegisterDialog.vue'

Vue.use(Vuetify)
Vue.config.productionTip = false

Vue.filter('date', DateFilter)
Vue.component('app-alert', Alert)
Vue.component('edit-meetup-details-dialog', EditeMeetupDetailsDialog)
Vue.component('edit-meetup-date-dialog', EditMeetupDateDialog)
Vue.component('edit-meetup-time-dialog', EditMeetupTimeDialog)
Vue.component('register-dialog', RegisterDialog)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyAnAkAZTiuLUBRkIxEjYYWX1fI4iJNVhyo',
      authDomain: 'dev-meetup-93b5a.firebaseapp.com',
      databaseURL: 'https://dev-meetup-93b5a.firebaseio.com',
      projectId: 'dev-meetup-93b5a',
      storageBucket: 'gs://dev-meetup-93b5a.appspot.com'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('fetchUserData')
      }
    })
    this.$store.dispatch('loadMeetups')
  }
})
